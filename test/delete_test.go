package test

import (
	"encoding/json"
	"github.com/eben/product/src"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestExclude(t *testing.T) {
	router := SetUpRouter()

	router.DELETE("/products", src.Exclude)

	req, _ := http.NewRequest("DELETE", "/products", nil)

	w := httptest.NewRecorder()
	router.ServeHTTP(w, req)

	body := gin.H{
		"Success": true,
		"Message": "Deletion was successful",
	}

	var response map[string]string
	_ = json.Unmarshal([]byte(w.Body.String()), &response)
	value, _ := response["Message"]

	assert.Equal(t, http.StatusOK, w.Code)
	assert.EqualValues(t, body["Message"], value)
}
