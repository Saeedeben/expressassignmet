package test

import (
	"bytes"
	"encoding/json"
	"github.com/eben/product/src"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestNewProduct(t *testing.T) {
	router := SetUpRouter()

	router.POST("/products", src.Store)

	product := []byte(`{
		"title_fa": "چیپس",
		"title_en": "chips",
		"description": "500 g",
		"price": 10000,
		"rating": 1,
		"location": {
			"lat": 35.665544,
			"long": 54.653215
		},
		"category_id": 1,
		"vendor_id": 1,
		"brand_id": 1,
		"stock": 4
		}`)

	req, _ := http.NewRequest("POST", "/products", bytes.NewBuffer(product))
	req.Header.Set("Content-Type", "application/json")

	w := httptest.NewRecorder()
	router.ServeHTTP(w, req)

	body := gin.H{
		"Success": true,
		"Message": "Store done successfully.",
	}

	var response map[string]string
	_ = json.Unmarshal([]byte(w.Body.String()), &response)
	value, _ := response["Message"]

	if w.Code != 200 {
		t.Error("status code is not equal 200")
	}

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, body["Message"], value)
}

func TestPurchase(t *testing.T) {
	router := SetUpRouter()

	router.PATCH("/products/:id/purchase", src.Purchase)

	req, _ := http.NewRequest("PATCH", "/products/1/purchase", nil)

	w := httptest.NewRecorder()
	router.ServeHTTP(w, req)

	body := gin.H{
		"Success": true,
		"Message": "Purchase done successfully",
	}

	var response map[string]string
	_ = json.Unmarshal([]byte(w.Body.String()), &response)
	value, _ := response["Message"]

	if w.Code != 200 {
		t.Error("status code is not equal 200")
	}

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, body["Message"], value)
}

func TestUpdateStocks(t *testing.T) {
	router := SetUpRouter()

	router.PUT("/products/:id/stocks", src.UpdateStocks)

	products := []byte(`{
			"0": {
				"product_id": 1,
				"stock": 10
			}
		}`)

	req, _ := http.NewRequest("PUT", "/products/1/stocks", bytes.NewBuffer(products))

	w := httptest.NewRecorder()
	router.ServeHTTP(w, req)

	body := gin.H{
		"Success": true,
		"Message": "Update multiple product was successful",
	}

	var response map[string]string
	_ = json.Unmarshal([]byte(w.Body.String()), &response)
	value, _ := response["Message"]

	if w.Code != 200 {
		t.Error("status code is not equal 200")
	}

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, body["Message"], value)
}
