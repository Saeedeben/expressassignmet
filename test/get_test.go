package test

import (
	"encoding/json"
	"github.com/eben/product/model"
	"github.com/eben/product/src"
	"github.com/eben/product/src/resources"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func SetUpRouter() *gin.Engine {
	gin.SetMode(gin.ReleaseMode)
	router := gin.Default()
	return router
}

func TestIndex(t *testing.T) {
	router := SetUpRouter()

	router.GET("/products", src.Index)
	req, _ := http.NewRequest("GET", "/products", nil)
	w := httptest.NewRecorder()
	router.ServeHTTP(w, req)

	var products []model.Product
	err := json.Unmarshal(w.Body.Bytes(), &products)
	if err != nil {
		return
	}

	assert.Equal(t, http.StatusOK, w.Code)
	assert.NotEmpty(t, products)
}

func TestShow(t *testing.T) {
	router := SetUpRouter()

	router.GET("/products/:id", src.Show)
	req, _ := http.NewRequest("GET", "/products/1", nil)
	w := httptest.NewRecorder()
	router.ServeHTTP(w, req)

	var product model.Product
	err := json.Unmarshal(w.Body.Bytes(), &product)
	if err != nil {
		return
	}

	assert.Equal(t, http.StatusOK, w.Code)
	assert.NotEmpty(t, product)
}

func TestVendorProducts(t *testing.T) {
	router := SetUpRouter()

	router.GET("/products/vendor/:vendorId", src.VendorProducts)
	req, _ := http.NewRequest("GET", "/products/vendor/1", nil)
	w := httptest.NewRecorder()
	router.ServeHTTP(w, req)

	var products map[string][]resources.ProductResponse
	err := json.Unmarshal(w.Body.Bytes(), &products)
	if err != nil {
		return
	}

	assert.Equal(t, http.StatusOK, w.Code)
	assert.NotEmpty(t, products)
}

//func TestGetDistance(t *testing.T) {
//	router := SetUpRouter()
//
//	router.GET("/products/distance", src.GetDistance)
//	req, _ := http.NewRequest("GET", "/products/distance", nil)
//	w := httptest.NewRecorder()
//	router.ServeHTTP(w, req)
//
//	var products map[string][]resources.ProductResponse
//	err := json.Unmarshal(w.Body.Bytes(), &products)
//	if err != nil {
//		return
//	}
//
//	assert.Equal(t, http.StatusOK, w.Code)
//	assert.NotEmpty(t, products)
//}
