package api

import (
	"github.com/eben/product/handler"
	"github.com/eben/product/src"
	"github.com/gin-gonic/gin"
)

func Router() {
	router := gin.Default()

	//store := persistence.NewInMemoryStore(time.Second)
	// Get
	router.GET("/products", src.Index)
	//api.GET("/products/vendor/:vendorId", cache.CachePage(store, 5*time.Minute, src.VendorProducts))
	router.GET("/products/vendor/:vendorId", src.VendorProducts)
	router.GET("/products/:id", src.Show)
	router.GET("/products/distance", src.GetDistance)

	// Store
	router.POST("/products", src.Store)
	router.POST("/products/file", src.UploadFile)

	// Updates
	router.PATCH("/products/:id/purchase", src.Purchase)
	router.PUT("/products/:id/stocks", src.UpdateStocks)

	// Delete
	router.DELETE("/products", src.Exclude)

	err := router.Run("localhost:8080")
	if err != nil {
		handler.CheckErr(err)
	}
}
