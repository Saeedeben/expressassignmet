package helpers

import (
	"github.com/patrickmn/go-cache"
	"time"
)

func GetCache(key string, cache *cache.Cache) (interface{}, bool) {
	var result interface{}
	data, found := cache.Get(key)
	if found {
		result = data
	}
	return result, found
}

func SetCache(cache *cache.Cache, key string, data interface{}, exp time.Duration) bool {
	cache.Set(key, data, exp)
	return true
}
