package ProductHelper

import (
	"encoding/json"
	"github.com/eben/product/model"
	"github.com/eben/product/src/resources"
	"gorm.io/datatypes"
	"math"
)

func GroupCategory(products []model.Product) map[string][]resources.ProductResponse {
	var responseProduct map[string][]resources.ProductResponse
	responseProduct = make(map[string][]resources.ProductResponse)
	for _, p := range products {
		response := resources.ShowResponse(&p)
		responseProduct[p.Category.Title] = append(responseProduct[p.Category.Title], response)
	}

	return responseProduct
}

func GetByDistance(products []model.Product, lat float64, lon float64, sort bool) ([]resources.ProductDistance, error) {
	var productByDist resources.ProductDistance
	var responseProducts []resources.ProductDistance
	for _, p := range products {

		location, err := productLocation(p.Location)
		if err != nil {
			return []resources.ProductDistance{}, err
		}

		dist := distance(lat, lon, location.Lat, location.Lon)
		productByDist = resources.ProductDistance{
			Id:            p.Id,
			TitleFa:       p.TitleFa,
			TitleEn:       p.TitleEn,
			Description:   p.Description,
			Price:         p.Price,
			Rating:        p.Rating,
			CategoryTitle: p.Category.Title,
			CategoryId:    p.CategoryId,
			ProductId:     p.Id,
			ProductTitle:  p.TitleFa,
			VendorId:      p.VendorId,
			Location:      p.Location,
			BrandTitleFa:  p.Brand.TitleFa,
			BrandId:       p.BrandId,
			Stock:         p.Stock,
			Distance:      dist,
		}
		responseProducts = append(responseProducts, productByDist)
	}

	if sort {
		response := sortByDistance(responseProducts)
		return response, nil
	}

	return responseProducts, nil
}

func productLocation(str datatypes.JSON) (model.Location, error) {
	var location model.Location

	err := json.Unmarshal(str, &location)
	if err != nil {
		return model.Location{}, err
	}

	return location, nil
}

func distance(lat1 float64, lon1 float64, lat2 float64, lon2 float64) float64 {
	radlat1 := float64(math.Pi * lat1 / 180)
	radlat2 := float64(math.Pi * lat2 / 180)

	theta := float64(lon1 - lon2)
	radtheta := float64(math.Pi * theta / 180)

	dist := math.Sin(radlat1)*math.Sin(radlat2) + math.Cos(radlat1)*math.Cos(radlat2)*math.Cos(radtheta)
	if dist > 1 {
		dist = 1
	}

	dist = math.Acos(dist)
	dist = dist * 180 / math.Pi
	dist = dist * 60 * 1.1515
	dist = dist * 1.609344

	return dist
}

func sortByDistance(products []resources.ProductDistance) []resources.ProductDistance {
	for i := 0; i < len(products); i++ {
		j := i
		for j > 0 {
			if products[j-1].Distance > products[j].Distance {
				products[j-1], products[j] = products[j], products[j-1]
			}
			j = j - 1
		}
	}

	return products
}
