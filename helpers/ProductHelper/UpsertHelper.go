package ProductHelper

import (
	"github.com/eben/product/model"
	"github.com/eben/product/src/validation"
	"strconv"
)

func StoreNewProduct(product *validation.NewProduct) *model.Product {
	return &model.Product{
		TitleFa:     product.TitleFa,
		TitleEn:     product.TitleEn,
		Description: product.Description,
		Price:       product.Price,
		Rating:      product.Rating,
		Location:    product.Location,
		CategoryId:  product.CategoryId,
		VendorId:    product.VendorId,
		BrandId:     product.BrandId,
		Stock:       product.Stock,
	}
}

func CreateProducts(data [][]string) []validation.ProductFile {
	var newP []validation.ProductFile
	for i := 0; i < len(data); i++ {
		if i > 0 {
			var product validation.ProductFile
			for index, field := range data[i] {
				switch index {
				case 0:
					product.TitleFa = field
				case 1:
					product.TitleEn = field
				case 2:
					product.Description = field
				case 3:
					product.Price, _ = strconv.ParseInt(field, 10, 0)
				case 4:
					product.Rating, _ = strconv.ParseInt(field, 10, 0)
				case 5:
					product.Location = field
				case 6:
					product.CategoryId, _ = strconv.ParseInt(field, 10, 0)
				case 7:
					product.VendorId, _ = strconv.ParseInt(field, 10, 0)
				case 8:
					product.BrandId, _ = strconv.ParseInt(field, 10, 0)
				case 9:
					product.Stock, _ = strconv.ParseInt(field, 10, 0)
				}
			}
			newP = append(newP, product)
		}
	}

	return newP
}
