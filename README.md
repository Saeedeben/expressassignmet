# ExpressAssignmet



## Getting started

This is my very first go project. So if it is not so good or complete.
Sorry about that.

To start this app you need user and password of your mysql database.

## Run and Deploy

To start application, run below command:
```
cd ./cmd
go run .
```
After that app will ask you some question for connect to database. Enter data and press enter key.

After connect database, application will create database and tables by default.
In this app seeding database will be done automatically.
After every running app will check the database existence, table creation, and fake data seeded or not.


## API, Requests

Postman file exist in project root folder and by using it you can find URLs and data.

## Test

For test project using command line, enter following command to run tests.
Please before running tests, run app once till data seed in database and tests work better.
```
export DBUSER="your database username"
export DBPASS="your database password"
cd ./test
go test -v
```

## Responsibilities

List of postman APIs, based on project description

 Create a single product -> /UPSERT/store Api

 [Optional] Create multiple products stored in a file -> /UPSERT/upload_file  

 [Optional] Change stock field of multiple products, specified by product_id -> /UPSERT/update_stocks

 Get a single product by id -> /GET/show

 Get all products of a vendor by vendor_id and have option to sort by rating -> /GET/index(filter)

 Get all products of a vendor and group them by their category -> /GET/index_vendor

 Get all nearby products by lat & long and have an option to sort by distance -> /GET/by_distance(filter)

 Purchase a product (this will decrease stock by 1) -> /UPSERT/purchase

 [Optional] Exclude all products that have zero stock -> /DELETE/delete

 Cache getting all products of a vendor, [Optional] this endpoint should not contain any
zero stock product -> /GET/index_vendor