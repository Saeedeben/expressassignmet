package main

import (
	"github.com/eben/product/api"
	"github.com/eben/product/cmd/pkg/input"
	"github.com/eben/product/cmd/pkg/output"
	"github.com/eben/product/cmd/pkg/repository"
	"github.com/eben/product/cmd/pkg/seeder"
	"github.com/eben/product/model"
	"github.com/gin-gonic/gin"
)

func main() {
	// Start App
	output.ArtPrinter(0)
	output.LnPrinter(1)

	// database, connection & tables & seeding
	input.GetConnectionData()
	repository.StartConnect()

	db := model.Database()
	model.Migrate(db)
	seeder.DbSeed()

	output.LnPrinter(10)
	output.LnPrinter(9)

	// Serve App
	gin.SetMode(gin.ReleaseMode)
	api.Router()
}
