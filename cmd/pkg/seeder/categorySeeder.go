package seeder

import (
	"github.com/eben/product/handler"
	"github.com/eben/product/model"
	"gorm.io/gorm"
)

var FakeCategories = []model.Category{
	{
		Title: "قهوه",
	},
	{
		Title: "لبنیات",
	},
	{
		Title: "بستنی",
	},
}

func SeedCategory(db *gorm.DB) {
	for i := 0; i < len(FakeCategories); i++ {
		if err := db.Create(&FakeCategories[i]).Error; err != nil {
			handler.ReturnError(err, 12)
		}
	}
}
