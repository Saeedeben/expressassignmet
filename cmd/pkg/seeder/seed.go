package seeder

import (
	"github.com/eben/product/cmd/pkg/output"
	"github.com/eben/product/model"
)

func DbSeed() {
	db := model.Database()

	err := SeedBrand(db)
	if err != nil {
		return
	}

	SeedCategory(db)
	SeedVendor(db)
	SeedProduct(db)

	output.LnPrinter(7)
	output.LnPrinter(9)
}
