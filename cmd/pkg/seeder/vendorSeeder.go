package seeder

import (
	"github.com/eben/product/handler"
	"github.com/eben/product/model"
	"gorm.io/gorm"
)

var FakeVendors = []model.Vendor{
	{
		Title:   "دریانی",
		Address: "سئول",
	},
	{
		Title:   "سوپرمارکت",
		Address: "شیخ بهایی",
	},
	{
		Title:   "ملاصدرا",
		Address: "پارک وی",
	},
}

func SeedVendor(db *gorm.DB) {
	for i := 0; i < len(FakeVendors); i++ {
		if err := db.Create(&FakeVendors[i]).Error; err != nil {
			handler.ReturnError(err, 12)
		}
	}
}
