package seeder

import (
	"github.com/eben/product/handler"
	"github.com/eben/product/model"
	"gorm.io/datatypes"
	"gorm.io/gorm"
)

var FakeProducts = []model.Product{
	{
		TitleFa:     "پودر قهوه لاواتزا (۲۵۰ گرم)",
		TitleEn:     "",
		Description: "250 گرم",
		Price:       259800,
		Rating:      5,
		Location:    datatypes.JSON("{\"lat\": 35.664444, \"lon\": 54.657215}"),
		CategoryId:  1,
		VendorId:    1,
		BrandId:     1,
		Stock:       4,
	},
	{
		TitleFa:     "پودر قهوه فکتوری (۲۵۰ گرم)",
		TitleEn:     "",
		Description: "250 گرم",
		Price:       299800,
		Rating:      5,
		Location:    datatypes.JSON("{\"lat\": 35.685544, \"lon\": 54.653285}"),
		CategoryId:  1,
		VendorId:    2,
		BrandId:     1,
		Stock:       4,
	},
	{
		TitleFa:     "پودر قهوه جیما (۲۵۰ گرم)",
		TitleEn:     "",
		Description: "250 گرم",
		Price:       219800,
		Rating:      8,
		Location:    datatypes.JSON("{\"lat\": 35.665544, \"lon\": 54.653215}"),
		CategoryId:  1,
		VendorId:    3,
		BrandId:     1,
		Stock:       1,
	},
	{
		TitleFa:     "پودر قهوه موآ (۲۵۰ گرم)",
		TitleEn:     "",
		Description: "250 گرم",
		Price:       319800,
		Rating:      3,
		Location:    datatypes.JSON("{\"lat\": 35.685544, \"lon\": 54.653215}"),
		CategoryId:  1,
		VendorId:    2,
		BrandId:     4,
		Stock:       2,
	},
	{
		TitleFa:     "پودر قهوه ست (۲۵۰ گرم)",
		TitleEn:     "",
		Description: "250 گرم",
		Price:       289800,
		Rating:      7,
		Location:    datatypes.JSON("{\"lat\": 35.685544, \"lon\": 54.653215}"),
		CategoryId:  1,
		VendorId:    2,
		BrandId:     5,
		Stock:       7,
	},
}

func SeedProduct(db *gorm.DB) {
	for i := 0; i < len(FakeProducts); i++ {
		if err := db.Create(&FakeProducts[i]).Error; err != nil {
			handler.ReturnError(err, 12)
		}
	}
}
