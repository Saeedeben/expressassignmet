package seeder

import (
	"errors"
	"github.com/eben/product/cmd/pkg/output"
	"github.com/eben/product/handler"
	"github.com/eben/product/model"
	"gorm.io/gorm"
)

var FakeBrands = []model.Brand{
	{
		TitleFa: "لاواتزا",
		TitleEn: "Lavazza",
	},
	{
		TitleFa: "فکتوری",
		TitleEn: "Factory",
	},
	{
		TitleFa: "جیما",
		TitleEn: "Gimma",
	},
	{
		TitleFa: "موآ",
		TitleEn: "MOA",
	},
	{
		TitleFa: "ست",
		TitleEn: "SET",
	},
}

func SeedBrand(db *gorm.DB) error {
	var brands model.Brand

	dbEx := db.Find(&brands, 1).RowsAffected

	if dbEx != 0 {
		output.LnPrinter(8)
		return errors.New("failed")
	}
	for i := 0; i < len(FakeBrands); i++ {
		if err := db.Create(&FakeBrands[i]).Error; err != nil {
			handler.ReturnError(err, 12)
		}
	}
	return nil
}
