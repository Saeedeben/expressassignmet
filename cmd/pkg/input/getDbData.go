package input

import (
	"bufio"
	"github.com/eben/product/cmd/pkg/output"
	"github.com/eben/product/handler"
	"os"
	"strings"
)

var reader = bufio.NewReader(os.Stdin)

// GetConnectionData for get database connection data
func GetConnectionData() {
	output.LnPrinter(9)
	output.LnPrinter(3)

	// fetch username

	output.Printer(4)
	dbUsername, err := reader.ReadString('\n')
	if err != nil {
		handler.ReturnError(err, 6)
	}

	// set username into the environment variables
	dbUsername = strings.Replace(dbUsername, "\n", "", -1)
	err = os.Setenv("DBUSER", dbUsername)
	if err != nil {
		handler.ReturnError(err, 7)
	}

	// fetch password

	output.Printer(5)
	dbPassword, passErr := reader.ReadString('\n')
	if passErr != nil {
		handler.ReturnError(passErr, 8)
	}

	// set password into the environment variables
	dbPassword = strings.Replace(dbPassword, "\n", "", -1)
	passErr = os.Setenv("DBPASS", dbPassword)
	if passErr != nil {
		handler.ReturnError(passErr, 9)
	}
}
