package output

import (
	"fmt"
	"github.com/common-nighthawk/go-figure"
)

var outputs = map[int]string{
	0:  "Welcome!",
	1:  "Please make sure you have MySQL installed on your system and served on port 3306.",
	2:  "DB/ Connected!",
	3:  "Please insert your database connection details.",
	4:  "Enter your database user: ",
	5:  "Enter your database password: ",
	6:  "Database created.",
	7:  "Database seeded successfully.",
	8:  "Database already seeded.",
	9:  "---------------------------------------",
	10: "Everything Ok and App is ready",
}

func LnPrinter(num int) {
	fmt.Println(outputs[num])
}

func Printer(num int) {
	fmt.Print(outputs[num])
}

func ArtPrinter(num int) {
	myFigure := figure.NewFigure(outputs[num], "smkeyboard", true)
	myFigure.Print()
}
