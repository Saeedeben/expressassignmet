package repository

import (
	"database/sql"
	"github.com/eben/product/cmd/pkg/output"
	"github.com/eben/product/handler"
	"github.com/go-sql-driver/mysql"
	"os"
)

var Db *sql.DB

func StartConnect() {

	Db = ConnectDB()

	output.LnPrinter(2)
	output.LnPrinter(9)

	defer CloseConnection(Db)

	existErr := CheckDBExistence(Db)
	if existErr != nil {
		// Database creation
		_, createDBErr := CreateDB(Db)
		if createDBErr != nil {
			handler.ReturnError(createDBErr, 3)
		}
	}
}

func ConnectDB() *sql.DB {
	// Capture connection properties
	config := mysql.Config{
		User:   os.Getenv("DBUSER"),
		Passwd: os.Getenv("DBPASS"),
		Net:    "tcp",
		Addr:   "127.0.0.1:3306",
	}

	// Get a database handle
	Db, err := sql.Open("mysql", config.FormatDSN())
	if err != nil {
		handler.ReturnError(err, 1)
	}

	pingErr := Db.Ping()
	if pingErr != nil {
		handler.ReturnError(pingErr, 2)
	}

	return Db
}

func CloseConnection(db *sql.DB) {
	err := db.Close()

	if err != nil {
		handler.ReturnError(err, 5)
	}
}
