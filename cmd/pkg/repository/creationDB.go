package repository

import (
	"database/sql"
	"github.com/eben/product/cmd/pkg/output"
	"github.com/eben/product/handler"
)

func CheckDBExistence(db *sql.DB) error {
	_, err := db.Exec(USE_DATABASE)
	if err != nil {
		return err
	}

	return nil
}

func CreateDB(db *sql.DB) (Db *sql.DB, error error) {
	_, err := db.Exec(CREATE_DATABASE)
	if err != nil {
		handler.ReturnError(err, 3)
	}

	output.LnPrinter(6)

	return db, nil
}
