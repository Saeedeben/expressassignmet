package model

import (
	"github.com/eben/product/handler"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"os"
)

func Database() *gorm.DB {
	DSN := os.Getenv("DBUSER") +
		":" +
		os.Getenv("DBPASS") +
		"@tcp(127.0.0.1:3306)/sql_product_ex_a?charset=utf8mb4&parseTime=True&loc=Local"

	db, err := gorm.Open(mysql.Open(DSN), &gorm.Config{
		PrepareStmt: true,
	})
	if err != nil {
		handler.CheckErr(err)
	}

	return db
}
