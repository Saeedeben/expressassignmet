package model

import (
	"github.com/eben/product/handler"
	"gorm.io/datatypes"
	"gorm.io/gorm"
)

func Migrate(db *gorm.DB) {
	err := db.AutoMigrate(&Brand{}, &Category{}, &Vendor{}, &Product{})
	if err != nil {
		handler.CheckErr(err)
	}
}

// Brand structure

type Brand struct {
	Id      int    `gorm:"primaryKey" json:"id"`
	TitleFa string `gorm:"not null" json:"title_fa"`
	TitleEn string `gorm:"not null" json:"title_en"`
	gorm.Model
}

// Category structure

type Category struct {
	Id    int    `gorm:"primaryKey" json:"id"`
	Title string `gorm:"not null" json:"title"`
	gorm.Model
}

// Vendor structure

type Vendor struct {
	Id      int    `gorm:"primaryKey" json:"id"`
	Title   string `gorm:"not null" json:"title"`
	Address string `json:"address"`
	gorm.Model
}

// Product Structure

type Product struct {
	Id          int            `gorm:"primaryKey" json:"id"`
	TitleFa     string         `gorm:"not null" json:"title_fa"`
	TitleEn     string         `json:"title_en" json:"title_en"`
	Description string         `gorm:"not null" json:"description"`
	Price       int            `gorm:"not null" json:"price"`
	Rating      int            `gorm:"not null" json:"rating"`
	Location    datatypes.JSON `gorm:"not null" json:"location"`
	CategoryId  int            `gorm:"not null" json:"category_id"`
	Category    Category       `gorm:"foreignKey:CategoryId" json:"category_title"`
	VendorId    int            `gorm:"not null" json:"vendor_id"`
	Vendor      Vendor         `gorm:"foreignKey:VendorId" json:"vendor_title"`
	BrandId     int            `gorm:"not null" json:"brand_id"`
	Brand       Brand          `gorm:"foreignKey:BrandId" json:"brand_fa"`
	Stock       int            `json:"stock"`
	gorm.Model
}

// Location Structure

type Location struct {
	Lat float64 `json:"lat"`
	Lon float64 `json:"lon"`
}
