package src

import (
	"github.com/eben/product/model"
	"github.com/gin-gonic/gin"
	"net/http"
)

func Exclude(c *gin.Context) {
	if result := model.Database().
		Table("products").
		Where("stock = ?", 0).
		Delete(&products).Error; result != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": ApiResponse("RNF", false)})
	}

	c.JSON(http.StatusOK, ApiResponse("DS", true))
}
