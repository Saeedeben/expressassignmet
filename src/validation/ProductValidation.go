package validation

import (
	"github.com/eben/product/model"
	"gorm.io/datatypes"
)

type NewProduct struct {
	TitleFa     string         `json:"title_fa" binding:"required"`
	TitleEn     string         `json:"title_en" binding:"required"`
	Description string         `json:"description"`
	Price       int            `json:"price" binding:"required"`
	Rating      int            `json:"rating" binding:"required"`
	Location    datatypes.JSON `json:"location" binding:"required"`
	CategoryId  int            `json:"category_id" binding:"required"`
	Category    model.Category
	VendorId    int `json:"vendor_id" binding:"required"`
	Vendor      model.Vendor
	BrandId     int `json:"brand_id" binding:"required"`
	Brand       model.Brand
	Stock       int `json:"stock" binding:"required"`
}

type BulkUpdate struct {
	PId   int `json:"product_id"`
	Stock int `json:"stock"`
}

type ProductFile struct {
	TitleFa     string `json:"title_fa" binding:"required"`
	TitleEn     string `json:"title_en" binding:"required"`
	Description string `json:"description"`
	Price       int64  `json:"price" binding:"required"`
	Rating      int64  `json:"rating" binding:"required"`
	Location    string `json:"location" binding:"required"`
	CategoryId  int64  `json:"category_id" binding:"required"`
	Category    model.Category
	VendorId    int64 `json:"vendor_id" binding:"required"`
	Vendor      model.Vendor
	BrandId     int64 `json:"brand_id" binding:"required"`
	Brand       model.Brand
	Stock       int64 `json:"stock" binding:"required"`
}
