package filters

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"net/http"
	"strconv"
)

func VendorIdFilter(vendorId string, db *gorm.DB, c *gin.Context) *gorm.DB {
	vendorIdInt, err := strconv.ParseInt(vendorId, 10, 0)
	if err != nil {
		c.JSON(http.StatusFailedDependency, gin.H{"error": gorm.ErrRecordNotFound.Error()})
	}

	return db.Where("vendor_id = ?", vendorIdInt)
}

func SortFilter(rSort string, db *gorm.DB) (*gorm.DB, error) {
	rateSort, sortErr := strconv.ParseBool(rSort)
	if sortErr != nil {
		return nil, sortErr
	}
	if rateSort {
		db = db.Order("rating desc")
	}

	return db, nil
}
