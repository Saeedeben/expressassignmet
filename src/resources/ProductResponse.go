package resources

import (
	"github.com/eben/product/model"
	"gorm.io/datatypes"
)

type ProductResponse struct {
	Id            int            `json:"id"`
	TitleFa       string         `json:"title_fa"`
	TitleEn       string         `json:"title_en"`
	Description   string         `json:"description"`
	Price         int            `json:"price"`
	Rating        int            `json:"rating"`
	CategoryTitle string         `json:"category_title"`
	CategoryId    int            `json:"category_id"`
	ProductId     int            `json:"product_id"`
	ProductTitle  string         `json:"product_title"`
	VendorId      int            `json:"vendor_id"`
	Location      datatypes.JSON `json:"location"`
	BrandTitleFa  string         `json:"brand_title_fa"`
	BrandId       int            `json:"brand_id"`
	Stock         int            `json:"stock"`
}

type ProductDistance struct {
	Id            int            `json:"id"`
	TitleFa       string         `json:"title_fa"`
	TitleEn       string         `json:"title_en"`
	Description   string         `json:"description"`
	Price         int            `json:"price"`
	Rating        int            `json:"rating"`
	CategoryTitle string         `json:"category_title"`
	CategoryId    int            `json:"category_id"`
	ProductId     int            `json:"product_id"`
	ProductTitle  string         `json:"product_title"`
	VendorId      int            `json:"vendor_id"`
	Location      datatypes.JSON `json:"location"`
	BrandTitleFa  string         `json:"brand_title_fa"`
	BrandId       int            `json:"brand_id"`
	Stock         int            `json:"stock"`
	Distance      float64        `json:"distance"`
}

func ShowResponse(p *model.Product) ProductResponse {
	return ProductResponse{
		p.Id,
		p.TitleFa,
		p.TitleEn,
		p.Description,
		p.Price,
		p.Rating,
		p.Category.Title,
		p.CategoryId,
		p.Id,
		p.TitleFa,
		p.VendorId,
		p.Location,
		p.Brand.TitleFa,
		p.BrandId,
		p.Stock,
	}
}
