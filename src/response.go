package src

type Response struct {
	Success bool
	Message string
}

var messages = map[string]string{
	"SD":   "Store done successfully.",
	"SDF":  "Store failed.",
	"RN":   "Record Doesnt Exist!",
	"RNF":  "Record Doesnt Exist or Not Has Enough Stock",
	"PRCF": "Purchase failed..!",
	"PRC":  "Purchase done successfully",
	"DS":   "Deletion was successful",
	"BUP":  "Update multiple product was successful",
	"FE":   "Fetch file error.",
	"SFE":  "Store file error",
	"FEXE": "File extension should be CSV.",
	"FS":   "File stored successfully",
	"RD":   "Request data doesnt fulfill requirement",
	"CD":   "Calculate Distance failed",
	"CSVE": "Read csv file failed.",
}

func ApiResponse(msg string, success bool) Response {
	return Response{
		success,
		messages[msg],
	}
}
