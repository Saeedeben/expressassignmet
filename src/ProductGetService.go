package src

import (
	"github.com/eben/product/helpers"
	"github.com/eben/product/helpers/ProductHelper"
	"github.com/eben/product/model"
	"github.com/eben/product/src/filters"
	"github.com/eben/product/src/resources"
	"github.com/gin-gonic/gin"
	"github.com/patrickmn/go-cache"
	"gorm.io/gorm/clause"
	"net/http"
	"strconv"
	"time"
)

var products []model.Product
var product model.Product
var CA = cache.New(5*time.Minute, 5*time.Minute)

func Index(c *gin.Context) {
	db := model.Database().Preload(clause.Associations)

	// if request has vendor id parameter
	if vendorId := c.Query("vendor_id"); vendorId != "" {
		db = filters.VendorIdFilter(vendorId, db, c)
	}

	// sort by rating parameter
	if ratingSort := c.Query("rate_sort"); ratingSort != "" {
		var filterErr error
		db, filterErr = filters.SortFilter(ratingSort, db)
		if filterErr != nil {
			c.JSON(http.StatusNotFound, gin.H{"error": ApiResponse("RD", false)})
			return
		}
	}

	if err := db.Find(&products).Error; err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": ApiResponse("RN", false)})
		return
	}

	var responseProduct []resources.ProductResponse
	for _, product := range products {
		response := resources.ShowResponse(&product)
		responseProduct = append(responseProduct, response)
	}

	c.JSON(http.StatusOK, responseProduct)
}

// this will output products based on vendor and groupBy category

func VendorProducts(c *gin.Context) {
	db := model.Database()

	if stockErr := db.Where("stock = ?", 0).Find(&products).Error; stockErr != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": stockErr})
		return
	}

	var responseProduct interface{}
	var found bool
	responseProduct, found = helpers.GetCache("vendor", CA)
	if len(products) != 0 || !found {
		db = db.Table("products").
			Preload(clause.Associations).
			Where("stock <> ?", 0)

		db = filters.VendorIdFilter(c.Param("vendorId"), db, c)

		if err := db.Find(&products).Error; err != nil {
			responseError := ApiResponse("RN", false)
			c.JSON(http.StatusNotFound, gin.H{"error": responseError})
			return
		}
		responseProduct = ProductHelper.GroupCategory(products)

		helpers.SetCache(CA, "vendor", responseProduct, 5*time.Minute)
	}

	c.JSON(http.StatusOK, responseProduct)
}

// get single product by id

func Show(c *gin.Context) {
	PId, err := strconv.ParseInt(c.Param("id"), 10, 0)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": ApiResponse("RD", false)})
		return
	}

	if result := model.Database().
		Preload(clause.Associations).
		First(&product, PId).Error; result != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": ApiResponse("RN", false)})
		return
	}

	c.JSON(http.StatusOK, resources.ShowResponse(&product))
}

// get bi distance

func GetDistance(c *gin.Context) {
	latString := c.Query("lat")
	lat, latErr := strconv.ParseFloat(latString, 64)
	if latErr != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": ApiResponse("RD", false)})
		return
	}

	lonString := c.Query("lon")
	lon, lonErr := strconv.ParseFloat(lonString, 64)
	if lonErr != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": ApiResponse("RD", false)})
		return
	}

	sortOrder := false
	sort := c.Query("sort")
	if sort != "" {
		var sortErr error
		sortOrder, sortErr = strconv.ParseBool(sort)
		if sortErr != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": ApiResponse("RD", false)})
			return
		}
	}

	if err := model.Database().
		Preload(clause.Associations).
		Find(&products).Error; err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": ApiResponse("RN", false)})
		return
	}

	responseProducts, calErr := ProductHelper.GetByDistance(products, lat, lon, sortOrder)
	if calErr != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": ApiResponse("CD", false)})
		return
	}

	c.JSON(http.StatusOK, responseProducts)

}
