package src

import (
	"encoding/csv"
	"github.com/eben/product/helpers/ProductHelper"
	"github.com/eben/product/model"
	"github.com/eben/product/src/validation"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"net/http"
	"path/filepath"
	"strconv"
)

// store a single product

func Store(c *gin.Context) {
	var product validation.NewProduct

	if err := c.ShouldBindJSON(&product); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": ApiResponse("RD", false)})
		return
	}

	newProduct := ProductHelper.StoreNewProduct(&product)

	if err := model.Database().Create(&newProduct).Error; err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": ApiResponse("RNF", false)})
	}

	c.JSON(http.StatusOK, ApiResponse("SD", true))
}

func Purchase(c *gin.Context) {
	PId, _ := strconv.ParseInt(c.Param("id"), 10, 0)
	db := model.Database().Table("products").
		Where("stock > ?", 0).
		Where("id = ?", PId)

	if result := db.First(&product).Error; result != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": gin.H{"error": ApiResponse("RNF", false)}})
		return
	}

	if result := db.Update("stock", gorm.Expr("stock - ?", 1)).Error; result != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": ApiResponse("SDF", false)})
		return
	}

	c.JSON(http.StatusOK, ApiResponse("PRC", true))
}

func UpdateStocks(c *gin.Context) {
	var products map[int]validation.BulkUpdate

	if err := c.ShouldBindJSON(&products); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": ApiResponse("RD", false)})
		return
	}

	db := model.Database().Table("products")

	for i := 0; i < len(products); i++ {
		db.Where("id = ?", products[i].PId).
			Update("stock", products[i].Stock)
	}

	c.JSON(http.StatusOK, ApiResponse("BUP", true))
}

func UploadFile(c *gin.Context) {
	file, header, err := c.Request.FormFile("products")
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": ApiResponse("FE", false)})
		return
	}

	fileExt := filepath.Ext(header.Filename)
	if fileExt != ".csv" {
		c.JSON(http.StatusBadRequest, gin.H{"error": ApiResponse("FEXE", false)})
		return
	}

	// read csv values using csv.Reader
	csvReader := csv.NewReader(file)
	data, csvErr := csvReader.ReadAll()
	if csvErr != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": ApiResponse("CSVE", false)})
		return
	}

	// convert records to array of structs
	newProduct := ProductHelper.CreateProducts(data)

	if storeError := model.Database().Table("products").
		Create(&newProduct).Error; storeError != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": ApiResponse("SFE", false)})
	}

	c.JSON(http.StatusOK, ApiResponse("FS", true))
}
