package handler

import (
	"fmt"
	"log"
)

var errors = map[int]string{
	1:  "DB/1 Connection failed!",
	2:  "DB/2 Ping error! Retry to create database",
	3:  "DB/3 Create database failed!",
	4:  "DB/4 Create database tables failed!",
	5:  "DB/5 Close database connection failed ...!",
	6:  "Fetching database username failed.",
	7:  "Set database username into the env failed.",
	8:  "Fetching database password failed.",
	9:  "Set database password into the env failed.",
	10: "Insert into database failed.",
	11: "Sorry we can not run application on this address",
	12: "Seeding failed!",
}

func ReturnError(err error, msg int) {
	fmt.Println(errors[msg])
	log.Fatalln(err.Error())
}

func CheckErr(err error) {
	if err != nil {
		panic(err)
	}
}
